#!/bin/bash

# 
# Start a netcat chat over libp2p network
#
# Nov 2021

endpoint="/libp2p/v1/listeners"
apiPort=8080

echo "[*] Obtaining Alice's DID and DID Document"
aDID=$(curl -s -X GET 127.0.0.1:${apiPort}/libp2p/v1/properties/did)
echo -e "[**] Alice's DID:\n$aDID"

aDIDDoc=$(curl -s -X GET 127.0.0.1:${apiPort}/libp2p/v1/properties/did-document)
echo -e "[**] Alice's DID Document:"
echo $aDIDDoc

data='{"Protocol":"/x/didcomm/v2","TargetAddress":"/ip4/127.0.0.1/tcp/3002"}'
curl -X POST -d ${data} 127.0.0.1:${apiPort}${endpoint}

echo "[*] Starting netcat chat. Don't close this session."
echo -e "[*] To start the chat, go to the bob example and run:\n./netcat-chat-over-libp2p.sh '${aDIDDoc}'"
echo -e "[*] Waiting for Bob :)"
nc -l 3002
