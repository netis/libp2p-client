# Netcat chat over a libp2p network

We demonstrate a chat via libp2p overlay network.
Communication between the clients is relayed.

## Setup

`alice` and `bob` are two pre-configured libp2p clients.
Both `alice` and `bob` listen for inbound connections on the libp2p network.

In our case, `bob` will connect with `alice` and start a chat.
