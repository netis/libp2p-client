#!/bin/bash

# 
# Start a netcat chat over libp2p network
#
# Nov 2021

endpoint="/libp2p/v1/listeners"
port=8081

echo "[*] Obtaining Bob's DID and DID Document"

bDID=$(curl -s -X GET 127.0.0.1:${port}/libp2p/v1/properties/did)
echo -e "[**] Bob's DID:\n$bDID"

bDIDDoc=$(curl -s -X GET 127.0.0.1:${port}/libp2p/v1/properties/did-document)
echo -e "[**] Bob's DID Document:"
echo $bDIDDoc

data='{"Protocol":"/x/didcomm/v2","TargetAddress":"/ip4/127.0.0.1/tcp/4002"}'
curl -X POST -d ${data} 127.0.0.1:${port}${endpoint}
# bActiveListeners=$(curl -s -X GET 127.0.0.1:${port}${endpoint})
# jq <<< $bActiveListeners


# aliceDIDDocument="$1"
aliceDIDDocument="/p2p/QmcSKqRQuoTDgMAebs9SLTiuLfAuNp8ePnBBJbgdrhDyiR/p2p-circuit/ipfs/12D3KooWNagc5ZPCJBj3dKdx1aFYYvYjUY5mu18mBBZWSh1K7uny"
# connect with Alice
# data='{"Protocol":"/x/didcomm/v2","ListenAddress":"/ip4/127.0.0.1/tcp/4445","DIDDocument":'${aliceDIDDocument}'}'

relay="12D3KooWED7tZH3H2JFHHF7kGq2pyNtq7JG9TunjjysCMXCULepV"
# Connect via a relay node
data='{"Protocol":"/x/didcomm/v2","ListenAddress":"/ip4/127.0.0.1/tcp/4445","TargetAddress":"/p2p/'${relay}'/p2p-circuit/ipfs/12D3KooWNagc5ZPCJBj3dKdx1aFYYvYjUY5mu18mBBZWSh1K7uny"}'

curl -X POST -d ${data} 127.0.0.1:${port}/libp2p/v1/connections

echo "[*] Starting netcat chat. Don't close this session."
# telnet -4 localhost 4445
nc localhost 4445
