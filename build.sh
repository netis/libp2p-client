#!/bin/bash

echo "[*] Building libp2p client"

export GOPATH=$(pwd)/go

GIT_COMMIT=$(git rev-list -1 HEAD)
btime=$(date)
echo "[*] $(/usr/local/go/bin/go version)"
echo "[*] Last Git Commit: ${GIT_COMMIT}"
echo "[*] Build time: $(date -u +%Y/%m/%d-%H:%M:%S-[UTC])"

/usr/local/go/bin/go build -o libp2p-client -ldflags "-X main.lastCommit=${GIT_COMMIT} -X main.buildTimestamp=$(date -u +%Y/%m/%d-%H:%M:%S-[UTC])" $@
