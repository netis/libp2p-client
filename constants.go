package main

import "time"

const (

	// P2PProtoPrefix is the default required prefix for protocol names
	P2PProtoPrefix = "/x/"

	cmgrTag = "stream-fwd"

	resolveTimeout = 10 * time.Second
)
