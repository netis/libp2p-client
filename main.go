package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	config "github.com/ipfs/go-ipfs-config"
	"github.com/ipfs/go-ipfs/core/bootstrap"
	log "github.com/sirupsen/logrus"
)

// main function
func main() {

	// Handle termination signals
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Show software info
	Banner()

	// Manage flags
	// Print help
	flag.Usage = func() {
		fmt.Println(Help)
		flag.PrintDefaults()
	}

	// Path to the client configuration file
	configNodePath := flag.String("conf.client", "config/client.json", "Path to the client configuration file")
	// Path to the libp2p networks configuration file
	configNetworkPath := flag.String("conf.network", "config/networks.json", "Path to the libp2p networks configuration file")
	// Address where we serve the endpoint
	listenAddress := flag.String("listen", "/ip4/127.0.0.1/tcp/3000", "Address which accepts inbound traffic and forwards it to the libp2p client. Format: multiaddress")
	// Address wehere we forward the incomming messages
	forwardAddress := flag.String("forward", "/ip4/127.0.0.1/tcp/4000", "Address to which the traffic from the libp2p network is forwarded to. Format: multiaddress")
	// targetAddress
	// targetAddress := flag.String("targetAddress", "/ip4/127.0.0.1/tcp/4000", "")

	// swarmAddress
	// libp2p swarm address used for libp2p discovery
	swarmAddress := flag.String("swarm", ":9000", "Local API endpoint")
	// apiAddress
	// REST API endpoint
	apiAddress := flag.String("api", ":8080", "Local API endpoint")
	// Protocol
	protocol := flag.String("protocol", "/x/didcomm/v1/demo", "Libp2p protocol name")
	// Verbosity
	verbosity := flag.String("v", "1", "Verbosity level: 1 - Error, 2 - Warn, 3 - Info, 4 - Debug, 5 - Trace")

	// Parse flags
	flag.Parse()

	// TODO: Update the verbosity level
	log.SetLevel(log.DebugLevel)

	// Print the default values
	log.Debug("Protocol:", protocol)
	log.Debug("Listen address:", *listenAddress)
	log.Debug("Forward address:", *forwardAddress)
	log.Debug("SwarmAddresses:", *swarmAddress)
	log.Debug("Client configuration file:", *configNodePath)
	log.Debug("Network configuration file:", *configNetworkPath)
	log.Debug("Verbosity:", *verbosity)

	// Load the libp2p network configuration
	configNetwork := ConfigNetwork{}
	configNetwork.Load(*configNetworkPath)

	// Load the libp2p node configuration
	configNode := ConfigNode{}
	configNode.Load(*configNodePath)

	// Initialise a new libp2p node
	node, err := NewLibP2PNode(&configNode, &configNetwork)
	if err != nil {
		panic(err)
	}

	// Create the node DID and DID Document
	err = node.NewNodeDIDDocument()
	if err != nil {
		panic(err)
	}
	log.Printf("%+v\n", node.DIDDocument)

	// Connect to the network
	// Parse bootstrap peers addresses
	bootstrapPeers, err := config.ParseBootstrapPeers(configNetwork.Bootstrap)
	if err != nil {
		log.Panic(err)
	}

	// Connect to bootstrap nodes
	_, err = bootstrap.Bootstrap(node.Host.ID(), node.Host, nil, bootstrap.BootstrapConfigWithPeers(bootstrapPeers))
	if err != nil {
		log.Panic(err)
	}

	mux := http.NewServeMux()

	mux.Handle("/libp2p/v1/", MakeHandler(node))

	http.Handle("/", accessControl(mux))

	// Run server
	go func() {
		if err := http.ListenAndServe(*apiAddress, nil); err != http.ErrServerClosed {
			// it is fine to use Fatal here because it is not main gorutine
			log.Fatalf("HTTP server ListenAndServe: %v", err)
		}
	}()

	// Handle os signals (terminate, etc.)
	sig := <-c
	fmt.Printf("Got %s signal. Aborting...\n", sig)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
