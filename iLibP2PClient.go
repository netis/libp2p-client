package main

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/hyperledger/aries-framework-go/pkg/doc/did"

	"github.com/go-kit/kit/endpoint"
)

// LibP2P node interfaces
type LibP2PNode interface {
	ListenTo(string, string) error
	ConnectTo(string, string, string, bool) error
	ListActiveListeners() ([]string, error)
	CloseListeners([]interface{}) error
	CloseStreams(bool, string) error
}

// ListenToRequest
// Libp2p listener arguments.
// <Protocol> specifies the libp2p handler name
// <TargetAddress> specifies the address to which connections made to the <Protocol> are forwarded to
//
// Example:
// Protocol: /x/test/v1
// TargetAddress: /ip/127.0.0.1/tcp/3000
// Forwards the libp2p connections to 127.0.0.1:3000
type ListenToRequest struct {
	// Libp2p protocol
	Protocol string `json:"Protocol"`
	// Local address
	TargetAddress string `json:"TargetAddress"`
}

// ListenToResponse
// Returns error message
type ListenToResponse struct {
	Err string `json:"err,omitempty"`
}

// ListenToEndpoint
// JSON-RPC endpoint that exposes the libp2p client ListenTo method
func ListenToEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		// Parse the request
		req := request.(ListenToRequest)
		// Create a new listener
		err := n.ListenTo(req.Protocol, req.TargetAddress)
		if err != nil {
			return ListenToResponse{err.Error()}, nil
		}
		return ListenToResponse{""}, nil
	}
}

// decodeListenToRequest
// Decodes the http request
func decodeListenToRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request ListenToRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

// ListActiveListenersRequest
// No input arguments
type ListActiveListenersRequest struct{}

// ListActiveListenersResponse
// Return a list of active libp2p listeners
type ListActiveListenersResponse struct {
	ActiveListeners []P2PListenerInfoOutput `json:"activeListeners,omitempty"`
	Err             string                  `json:"err,omitempty"`
}

// ListActiveListenersEndpoint
// Method that lists active libp2p listeners
func ListActiveListenersEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		// Get a list of active listeners
		activeListeners, err := n.listActiveListeners()
		if err != nil {
			return ListActiveListenersResponse{[]P2PListenerInfoOutput{}, err.Error()}, nil
		}
		return ListActiveListenersResponse{activeListeners.Listeners, ""}, nil
	}
}

// decodeListActiveListenersRequest
func decodeListActiveListenersRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return ListActiveListenersRequest{}, nil
}

type CloseListenersRequest struct{}
type CloseListenersResponse struct{}

// ConnectToRequest
// Arguments to create a libp2p connection
// <Protocol> specifies the libp2p handler name
// <ListenAddress> specifies the address from which the connection is forwarded
// <TargetAddress> specifies the address to which the connection is forwarded
// <AllowCustom>
// <DID> specifies the DID of the client we want to connect with
// <DIDDocument> DID Document of the client we want to conenct with
// Only one of: TargetAddress, DID, DIDDocument MUST be specified
type ConnectToRequest struct {
	Protocol      *string `json:"Protocol,omitempty"`
	ListenAddress string
	TargetAddress *string `json:"TargetAddress,omitempty"`
	AllowCustom   bool
	DID           *string  `json:"DID,omitempty"`
	DIDDocument   *did.Doc `json:"DIDDocument,omitempty"`
}

// decodeListenToRequest
// Decodes the http request
func decodeConnectToRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var request ConnectToRequest
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return nil, err
	}
	return request, nil
}

// ConnectToResponse
// Returns error message
type ConnectToResponse struct {
	Err string `json:"err,omitempty"`
}

// ConnectToEndpoint
// JSON-RPC endpoint that handles the new libp2p connections
func ConnectToEndpoint(n *Node) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(ConnectToRequest)
		// Extract the target address
		protocol, targetAddress, err := getTargetConnectionInfo(req)
		if err != nil {
			return ConnectToResponse{err.Error()}, nil
		}
		// Try to establish a connection with the target address
		err = n.ConnectTo(protocol, req.ListenAddress, targetAddress, req.AllowCustom)
		if err != nil {
			return ConnectToResponse{err.Error()}, nil
		}
		return ConnectToResponse{""}, nil
	}
}

// extract the target address
// TODO: Error Handling
func getTargetConnectionInfo(req ConnectToRequest) (string, string, error) {
	var targetAddress, protocol string
	if req.DIDDocument != nil {
		// 1. Check if DID Document is set
		service, _ := did.LookupService(req.DIDDocument, "DIDCommMessaging")
		if service != nil {
			targetAddress = strings.TrimLeft(service.ServiceEndpoint, "libp2p:")
			protocol = "/x/didcomm/v2"
		}
	} else if req.DID != nil {
		// 2. Check if DID is set
		return "", "", errors.New("[ERROR] DID Document resolution is not supported, yet)")
	} else if req.TargetAddress != nil {
		// 3. Check if TargetAddress is set
		protocol = *req.Protocol
		targetAddress = *req.TargetAddress
	} else {
		return "", "", errors.New("[ERROR] Missing TargetAddress (DID, DID Document or PeerID)")
	}

	return protocol, targetAddress, nil
}

type CloseStreamsRequest struct{}
type CloseStreamsResponse struct{}

// encodeResponse
// Encode the method response
func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}
